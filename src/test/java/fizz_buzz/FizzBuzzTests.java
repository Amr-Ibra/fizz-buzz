package fizz_buzz;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FizzBuzzTests {

  FizzBuzz fizzBuzz = new FizzBuzz();

  @Test
  public void shouldReturnFizz_WhenArgDivisableBy3Not5() {
    final int arg = 9;
    final String expected = "Fizz";
    final String actual = this.fizzBuzz.returnFizzOrBuzz(arg);
    assertEquals(expected, actual);
  }

  @Test
  public void shouldReturnBuzz_WhenArgDivisableBy5Not3() {
    final int arg = 20;
    final String expected = "Buzz";
    final String actual = this.fizzBuzz.returnFizzOrBuzz(arg);
    assertEquals(expected, actual);
  }

  @Test
  public void shouldReturnFizzBuzz_WhenArgDivisableBy3And5() {
    final int arg = 30;
    final String expected = "FizzBuzz";
    final String actual = this.fizzBuzz.returnFizzOrBuzz(arg);
    assertEquals(expected, actual);
  }

  @Test
  public void shouldReturnNumAsStr_WhenArgNotDivisableBy3Nor5() {
    final int arg = 7;
    final String expected = Integer.toString(arg);
    final String actual = this.fizzBuzz.returnFizzOrBuzz(arg);
    assertEquals(expected, actual);
  }

}
